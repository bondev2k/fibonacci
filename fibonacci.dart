void main() {
  var i = fibonacci(8); //0,1,1,2,3,5,8,13,21
  print(i);
}

int fibonacci(int x) {
  if (x < 2) {
    return x; // x=0 in ra  0, x=1 in ra 1
  } else {
    return fibonacci(x - 1) + fibonacci(x - 2); // x=8 => x>2 = f(7) + f(6) = 21
  }
}
